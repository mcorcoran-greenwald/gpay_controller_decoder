using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using GPayControllerDecoder.models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace GPayControllerDecoder
{
    public static class ControllerByGraphic
    {
        [FunctionName("ControllerGraphicTypes")]
        public static async Task<IActionResult> GetGraphicsTypes(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ExecutionContext context,
            ILogger log)
        {
            var graphics = GetControllerGraphics(context);
            var uniqueTypes = graphics.Select(e => e.Type).Distinct().ToList();
            return new OkObjectResult(uniqueTypes);
        }

        [FunctionName("ControllerGraphics")]
        public static async Task<IActionResult> GetControllerGraphicByType(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ExecutionContext context,
            ILogger log)
        {
            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            dynamic data = JsonConvert.DeserializeObject(requestBody);
            string type = data?.type;


            var graphics = GetControllerGraphics(context);
            var uniqueTypes = graphics.Where(x => x.Type == type).ToList();

            return new OkObjectResult(uniqueTypes);
        }
        private static List<ControllerImage> GetControllerGraphics(ExecutionContext context)
        {
            var filePath = $"{Directory.GetParent(context.FunctionDirectory).FullName}\\image-db.json";
            using (StreamReader r = new StreamReader(filePath))
            {
                string json = r.ReadToEnd();
                List<ControllerImage> items = JsonConvert.DeserializeObject<List<ControllerImage>>(json);
                return items;
            }

        }
    }
}
