﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GPayControllerDecoder.models
{
    public class GPayKit
    {
        public int NumberOfKitsNeeded { get; set; }
        public GPayPartNo RecommendedPartNo { get; set; }
        public List<GPayPartNo> AlternativePartNos { get; set; }
        public bool Supported { get; set; }
    }

    public class GPayPartNo
    {
        private GPayPartNo(string value) { Value = value; }
        public string Value { get; set; }
        public static GPayPartNo MdcCoinKit { get { return new GPayPartNo("4004-001-2 MDC/Quantum - Dual Coin"); } }
        public static GPayPartNo MdcCardKit { get { return new GPayPartNo("4004-002-2 MDC/Quantum - Stand-alone (Card Ready) - Includes 24v power filter "); } }
        public static GPayPartNo AcaWithOutPower { get { return new GPayPartNo("4004-101-2 ACA - Dual Coin Without Power"); } }
        public static GPayPartNo QgpWithOutPower { get { return new GPayPartNo("4004-101-3 ACA+QGP - Dual Coin Without Power"); } }
        public static GPayPartNo Aca24vPower { get { return new GPayPartNo("4004-102-2 ACA - Dual Coin With 24v Power filter"); } }
        public static GPayPartNo Qgp24vPower { get { return new GPayPartNo("4004-102-3 ACA+QGP - Dual Coin With 24v Power filter"); } }
        public static GPayPartNo AcaLinePower { get { return new GPayPartNo("4004-103-2 ACA - Dual Coin With 120v Line Power filter"); } }
        public static GPayPartNo QgpLinePower { get { return new GPayPartNo("4004-103-3 ACA+QGP - Dual Coin With 120v Line Power filter"); } }
        public static GPayPartNo MayTagPRKit { get { return new GPayPartNo("4003-001-2 Maytag PR - Stand Alone"); } }
        public static GPayPartNo MayTagPDKit { get { return new GPayPartNo("4003-002-2 Maytag PD - Dual Coin"); } }
        public static GPayPartNo LGCardKit { get { return new GPayPartNo("4016-001-2 LG Stand Alone"); } }
        public static GPayPartNo GPayPlusContactReader { get { return new GPayPartNo("4116-001 GPay Plus with Contact Reader Kit "); } }
        public static GPayPartNo LGContactReader { get { return new GPayPartNo("2016-005 LG Washer Dryer Contact Card Reader Kit"); } }

        public static GPayPartNo InDevelopment { get { return new GPayPartNo("In Development, soon to be supported"); } }
    }
}
