﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GPayControllerDecoder.models
{
    public class ControllerImage
    {
        public string Image { get; set; }
        public string Type { get; set; }
        public string Version { get; set; }
        public string Description { get; set; }
        
    }
}
