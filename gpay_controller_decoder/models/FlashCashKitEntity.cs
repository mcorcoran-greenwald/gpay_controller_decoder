﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Text;

namespace GPayControllerDecoder.models
{
    public class FlashCashKitEntity: TableEntity
    {
        public string Kit { get; set; }
    }
}
