﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;
using GPayControllerDecoder.models;


namespace GPayControllerDecoder
{
    public class ControllerResponse
    {
        public string ResponseType { get; set; }
        public string Brand { get; set; }
        public string Product { get; set; }
        public string Protocol { get; set; }
        public string ProfileRequired { get; set; }
        public string Heater { get; set; }
        public string ControllerType { get; set; }
        public string Actuation { get; set; }
        public string Speed { get; set; }
        public string Cylinder { get; set; }
        public string Door { get; set;  }
        public string Design { get; set; }
        public string Market { get; set; }
        public string Description { get; set; }
        public string OtherOptions { get; set; }
        public string Washbasket { get; set; }
        public string HeatSource { get; set; }
        public string Color { get; set; }
        public string Voltage { get; set; }
        public string ErrorMessage { get; set; }

        public GPayKit GPayKit { get; set; }
        public FlashCashKit FlashCashKit { get; set; }
    }
}
