﻿using GPayControllerDecoder.models;
using System;
using System.Collections.Generic;
using System.Text;

namespace GPayControllerDecoder.machines
{
    public class MayTag
    {
        public static ControllerResponse GetController(string modelNumber)
        {
            modelNumber = modelNumber.ToLower();
            char[] charArr = modelNumber.ToCharArray();
            ControllerResponse controller = new ControllerResponse();
            if (modelNumber == "unsupported")
            {
                controller.ResponseType = "success";
                return controller;
            }
            if (charArr[5].ToString() != "p")
            {
                controller.ResponseType = "failure";
                return controller;
            }
            if (charArr[6].ToString().IndexOfAny(new char[] { 'd', 'r' }) <
                0)
            {
                controller.ResponseType = "failure";
                return controller;
            }

            controller.ResponseType = "success";
            return controller;
        }

        public static GPayKit GetGPayKit(string modelNumber)
        {
            modelNumber = modelNumber.ToLower();
            GPayKit kit = new GPayKit();
            kit.AlternativePartNos = new List<GPayPartNo>();
            kit.Supported = true;
            kit.NumberOfKitsNeeded = 1;
            var modelType = modelNumber.Substring(0, 3);
            if (modelType == "mfr" || modelType == "mxr" || modelNumber == "unsupported")
            {
                kit.RecommendedPartNo = GPayPartNo.InDevelopment;
                kit.NumberOfKitsNeeded = 0;
                return kit;
            }

            var machineType = modelNumber.Substring(5, 2);
            if (machineType == "pr")
            {
                kit.RecommendedPartNo = GPayPartNo.MayTagPRKit;
            }
            else if (machineType == "pd")
            {
                kit.RecommendedPartNo = GPayPartNo.MayTagPDKit;
            }
            else
            {
                kit.Supported = false;
            }
            return kit;
        }

    }
}
