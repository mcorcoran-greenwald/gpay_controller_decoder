﻿using GPayControllerDecoder.models;
using System;
using System.Collections.Generic;
using System.Text;

namespace GPayControllerDecoder.machines
{
    public class Whirlpool
    {
        public static ControllerResponse GetController(string modelNumber)
        {
            
            ControllerResponse controller = new ControllerResponse();
            modelNumber = ConvertWhirlpoolToMaytag(modelNumber);
            return MayTag.GetController(modelNumber);
        }

        private static string ConvertWhirlpoolToMaytag(string modelNumber)
        {
            modelNumber = modelNumber.ToUpper();
            switch (modelNumber)
            {
                case "CET9000GQ":
                    modelNumber = "MLE20PD";
                    break;
                case "CET9100GQ":
                    modelNumber = "MLE20PR";
                    break;
                case "CGT9000GQ":
                    modelNumber = "MLG20PD";
                    break;
                case "CGT9100GQ":
                    modelNumber = "MLG20PR";
                    break;
                case "CAE2745FQ":
                    modelNumber = "unsupported";
                    break;
            }
            return modelNumber;
        }
        public static GPayKit GetGPayKit(string modelNumber)
        {
            modelNumber = ConvertWhirlpoolToMaytag(modelNumber);
            return MayTag.GetGPayKit(modelNumber);
        }
    }
}
