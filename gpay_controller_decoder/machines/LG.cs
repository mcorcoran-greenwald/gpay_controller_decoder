﻿using GPayControllerDecoder.models;
using System;
using System.Collections.Generic;
using System.Text;

namespace GPayControllerDecoder.machines
{
    public class LG
    {

        public static ControllerResponse GetController(string modelNumber)
        {
            modelNumber = modelNumber.ToUpper();
            ControllerResponse prod = new ControllerResponse();
            prod.ResponseType = "success";
            switch (modelNumber)
            {
                case "TCW2013QS1":
                case "TCW2013CS1":
                    prod.Product = "Titan - Original";
                    prod.Description = "Washer";
                    prod.Protocol = "LG Rev 1.7";
                    prod.ProfileRequired = "LG 2019";
                    break;
                case "GCWL1069QS5":
                case "GCWL1069CS5":
                case "GCWL1069QD5":
                case "GCWL1069CD5":
                case "GCWL1069QS1":
                case "GCWL1069CS1":
                    prod.Product = "Giant Fast - WIFI 1.0";
                    prod.Description = "Washer";
                    prod.Protocol = "LG Rev 1.7";
                    prod.ProfileRequired = "LG 2019";
                    break;
                case "GDL1329QGS3":
                case "GDL1329CGS3":
                case "GDL1329QGW3":
                case "GDL1329CGW3":
                case "GDL1329QGD3":
                case "GDL1329CGD3":
                case "GDL1329QES3":
                case "GDL1329CES3":
                case "GDL1329QEW3":
                case "GDL1329CEW3":
                case "GDL1329QED3":
                case "GDL1329CED3":
                    prod.Product = "Giant Fast - WIFI 1.0";
                    prod.Description = "Dryer";
                    prod.Protocol = "LG Rev 1.7";
                    prod.ProfileRequired = "LG 2019";
                    break;
                case "GCWF1069QS3":
                case "GCWF1069CS3":
                case "GCWF1069QD3":
                case "GCWF1069CD3":
                case "GCWF1069QS1":
                case "GCWF1069CS1":
                case "GCWF1069QD1":
                case "GCWF1069CD1":
                    prod.Product = "Giant Pro";
                    prod.Description = "Washer";
                    prod.Protocol = "LG Rev 1.7";
                    prod.ProfileRequired = "LG 2019";
                    break;
                case "GDP1329QGS":
                case "GDP1329CGS":
                case "GDP1329QGW":
                case "GDP1329CGW":
                case "GDP1329QGD":
                case "GDP1329CGD":
                case "GDP1329QES":
                case "GDP1329CES":
                case "GDP1329QEW":
                case "GDP1329CEW":
                case "GDP1329QED":
                case "GDP1329CED":
                    prod.Product = "Giant Pro";
                    prod.Description = "Dryer";
                    prod.Protocol = "LG Rev 1.7";
                    prod.ProfileRequired = "LG 2019";
                    break;
                case "TCWM2013QD7":
                case "TCWM2013CD7":
                case "TCWM2013QS7":
                case "TCWM2013CS7":
                    prod.Product = "Titan Max - WIFI 2.0";
                    prod.Description = "Washer";
                    prod.Protocol = "LG Rev 1.8";
                    prod.ProfileRequired = "LG 2020";
                    break;
                case "GCWM1069QS7":
                case "GCWM1069CS7":
                case "GCWM1069QD7":
                case "GCWM1069CD7":
                    prod.Product = "Giant Max - WIFI 2.0";
                    prod.Description = "Washer";
                    prod.Protocol = "LG Rev 1.8";
                    prod.ProfileRequired = "LG 2020";
                    break;
                case "GDL1329QGS7":
                    prod.Product = "Giant Max - WIFI 2.0";
                    prod.Description = "Dryer";
                    prod.Protocol = "LG Rev 1.8";
                    prod.ProfileRequired = "LG 2020";
                    break;
                case "GDL1329CGS7":
                case "GDL1329QGW7":
                case "GDL1329CGW7":
                case "GDL1329QGD7":
                case "GDL1329CGD7":
                case "GDL1329QES7":
                case "GDL1329CES7":
                case "GDL1329QEW7":
                case "GDL1329CEW7":
                case "GDL1329QED7":
                case "GDL1329CED7":
                    prod.Product = "Giant Max Dryer - WIFI 2.0";
                    prod.Description = "Dryer";
                    prod.Protocol = "LG Rev 1.8";
                    prod.ProfileRequired = "LG 2020";
                    break;
                case "TCD1840QGS7":
                case "TCD1840CGS7":
                case "TCD1840QGW7":
                case "TCD1840CGW7":
                case "TCD1840QGD7":
                case "TCD1840CGD7":
                case "TCD1840QES7":
                case "TCD1840CES7":
                case "TCD1840QEW7":
                case "TCD1840CEW7":
                case "TCD1840QED7":
                case "TCD1840CED7":
                    prod.Product = "Titan Light - WIFI 2.0";
                    prod.Description = "Dryer";
                    prod.Protocol = "LG Rev 1.8";
                    prod.ProfileRequired = "LG 2020";
                    break;
                default:
                    prod.Protocol = "LG Rev <= 1.6";
                    prod.ProfileRequired = "Original LG";
                    break;
            }
            return prod;
        }

        public static GPayKit GetGPayKit(string modelNumber)
        {
            modelNumber = modelNumber.ToLower();
            GPayKit kit = new GPayKit();
            var altParts = new List<GPayPartNo>();
            altParts.Add(GPayPartNo.GPayPlusContactReader);
            altParts.Add(GPayPartNo.LGContactReader);
            kit.AlternativePartNos = altParts;
            kit.Supported = true;
            kit.NumberOfKitsNeeded = 1;
            kit.RecommendedPartNo = GPayPartNo.LGCardKit;
            return kit;
        }
    }
}
