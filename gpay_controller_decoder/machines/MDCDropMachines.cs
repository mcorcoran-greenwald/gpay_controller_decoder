﻿using GPayControllerDecoder.models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace GPayControllerDecoder.machines
{
    class MDCDropMachines
    {
        public static ControllerResponse GetController(string modelNumber)
        {
            modelNumber = modelNumber.ToUpper();
            char[] charArr = modelNumber.ToCharArray();
            ControllerResponse controller = new ControllerResponse();

            if (modelNumber.StartsWith("BSEBCFGS171TW01")
                || modelNumber.StartsWith("BSGBCFGS111TW01")
                || IsValidRegex(modelNumber, @"(HSET17[A-Z 0-9]F)")
                || IsValidRegex(modelNumber, @"(HSEZ17[A-Z 0-9]F)")
                || IsValidRegex(modelNumber, @"(HSGT19[A-Z 0-9]F)")
                || IsValidRegex(modelNumber, @"(HSGZ19[A-Z 0-9]F)")
                || IsValidRegex(modelNumber, @"(SSET07[A-Z 0-9]F)")
                || IsValidRegex(modelNumber, @"(SSET17[A-Z 0-9]F)")
                || IsValidRegex(modelNumber, @"(SSEX07[A-Z 0-9]F)")
                || IsValidRegex(modelNumber, @"(SSEZ07[A-Z 0-9]F)")
                || IsValidRegex(modelNumber, @"(SSEZ17[A-Z 0-9]F)")
                || IsValidRegex(modelNumber, @"(SSGT09[A-Z 0-9]F)")
                || IsValidRegex(modelNumber, @"(SSGT19[A-Z 0-9]F)")
                || IsValidRegex(modelNumber, @"(SSGX09[A-Z 0-9]F)")
                || IsValidRegex(modelNumber, @"(SSGX19[A-Z 0-9]F)")
                || IsValidRegex(modelNumber, @"(SSGZ09[A-Z 0-9]F)")
                || IsValidRegex(modelNumber, @"(SSGZ19[A-Z 0-9])")
                
               )
            {
                controller.ControllerType = "MDC";
                switch (charArr[0].ToString())
                {
                    case "H":
                        controller.Brand = "Huebsch";
                        break;                   
                    case "S":
                        controller.Brand = "Speed Queen";
                        break;
                }
                controller.ResponseType = "success";
                return controller;
            }

            controller.ResponseType = "failure";
            return controller;



        }

        private static bool IsValidRegex(string input, string pattern)
        {
            if (string.IsNullOrEmpty(pattern)) return false;

            try
            {
                var match = Regex.Match(input, pattern);
                return match.Success;
            }
            catch (ArgumentException)
            {
                return false;
            }

            
        }
        public static GPayKit GetGPayKit(string modelNumber)
        {
            modelNumber = modelNumber.ToLower();
            GPayKit kit = new GPayKit();
            kit.AlternativePartNos = new List<GPayPartNo>();
            kit.Supported = true;
            kit.NumberOfKitsNeeded = 2;

            kit.RecommendedPartNo = GPayPartNo.MdcCardKit;
            return kit;
        }

    }
}

