﻿using GPayControllerDecoder.models;
using System;
using System.Collections.Generic;
using System.Text;

namespace GPayControllerDecoder.machines
{
    public class AllianceOld8Char
    {
        public static ControllerResponse GetController(string modelNumber)
        {
            //string val = null;
            modelNumber = modelNumber.ToLower();
            char[] charArr = modelNumber.ToCharArray();
            ControllerResponse controller = new ControllerResponse();
            if (modelNumber.Length < 8)
            {
                controller.ResponseType = "failure";
                return controller;
            }
            var Brand = charArr[0].ToString();
            var ProductClass = charArr[1].ToString();
            var WasherDescription = charArr[2].ToString();
            var ControlOption = charArr[3].ToString();
            var WasherSpeeds = charArr[4].ToString();
            var Washbasket = charArr[5].ToString();
            var Color = charArr[6].ToString();
            var Design = charArr[7].ToString();

            //First Position - Brand
            switch (Brand)
            {
                case "s":
                    controller.Brand = "Speed Queen";
                    break;
                case "h":
                    controller.Brand = "Huebsch";
                    break;
                case "u":
                    controller.Brand = "Unimac";
                    break;
                case "b":
                    controller.Brand = "IPSO";
                    break;
                case "f":
                    controller.Brand = "Fagor";
                    break;
                case "g":
                    controller.Brand = "Girbau";
                    break;
                case "j":
                    controller.Brand = "Continental (dryer)";
                    break;
                case "p":
                    controller.Brand = "Primus";
                    break;
                default:
                    controller.ResponseType = "failure";
                    return controller;
            }


            //Second Position - Product Class

            switch (ProductClass)
            {
                case "w":
                    controller.Product = "Washer";
                    break;
                case "d":
                    controller.Product = "Dryer";
                    break;
                case "f":
                    controller.Product = "Dryer - Front Controls";
                    break;
                case "s":
                    controller.Product = "Stacked Dryer";
                    break;
                case "t":
                    controller.Product = "Stacked Washer/Dryer";
                    break;
                case "u":
                    controller.Product = "Upper Dryer for Stack";
                    break;
                default:
                    controller.ResponseType = "failure";
                    return controller;
            }

            //Third Position - Description/Heater
            switch (WasherDescription)
            {
                case "t":
                    controller.Description = "Washer, Topload";
                    break;
                case "l":
                    controller.Description = "TLW, Small Load switch, lg tub & cover, 3.15 cu ft.";
                    break;
                case "f":
                    controller.Description = "Washer, Frontload - (w/front controls)";
                    break;
                case "r":
                    controller.Description = "Washer, Frontload - (w/rear controls)";
                    break;
                case "e":
                    controller.Heater = "Electric";
                    break;
                case "g":
                    controller.Heater = "Gas";
                    break;
                default:
                    controller.ResponseType = "failure";
                    return controller;
            }
     

            //Forth Position - Control
            switch (ControlOption)
            {
                case "0":
                    controller.ControllerType = "E-mech, Push-To-Start Operation";
                    break;
                case "1":
                    controller.ControllerType = "E-mech, Prep for Coin slide - (short cycle washer)";
                    break;
                case "2":
                    controller.ControllerType = "E-mech, Prep for Coin slide - (long cycle washer)";
                    break;
                case "4":
                    controller.ControllerType = "Electronic, (EDC) Prep for Coin Drop OR Prep for Any Electronic if a NUE or NUG";
                    break;
                case "5":
                    controller.ControllerType = "Electronic, (EDC) Coin Drop Installed";
                    break;
                case "6": 
                    controller.ControllerType = "Electronic, (EDC) Prep for Card Reader";
                    break;
                case "8":
                    controller.ControllerType = "Electronic e-mech substitute, Push-To-Start operation ";
                    break;
                case "9":
                    controller.ControllerType = "Electronic e-mech substitute, Prep for Coin Slide";
                    break;
                case "a":
                    controller.ControllerType = "Electronic, (NetMaster) Prep for Coin Drop";
                    break;
                case "b":
                    controller.ControllerType = "Electronic, (NetMaster) Coin Drop Installed";
                    break;
                case "c":
                    controller.ControllerType = "Electronic, (NetMaster) Prep for Card Reader";
                    break;
                case "f":
                    controller.ControllerType = "Electronic, (NetMaster) Integrated Card Reader Installed";
                    break;
                case "l":
                    controller.ControllerType = "Basic Electronic, (MDC) Central Pay / No Vend";
                    break;
                case "r":
                    controller.ControllerType = "Basic Electronic, (MDC) World Drop Installed";
                    break;
                case "t":
                    controller.ControllerType = "Basic Electronic, (MDC) Coin Drop Installed (replaces Z)";
                    break;
                case "v":
                    controller.ControllerType = "E-mech, Prep for Coin Slide, Short Cycle Water Saver/Temp Control Washer";
                    break;
                case "w":
                    controller.ControllerType = "E-mech, Prep for Coin Slide, Long Cycle Water Saver/Temp Control Washer";
                    break;
                case "x":
                    controller.ControllerType = "Basic Electronic, (MDC) Prep for Coin Drop";
                    break;
                case "y":
                    controller.ControllerType = "Basic Electronic, (MDC) Prep for Card";
                    break;
                default:
                    controller.ResponseType = "failure";
                    return controller;
            }
 
            //Fith Position - Speeds/Other Options
            switch (WasherSpeeds)
            {
                case "1" when (ProductClass == "w"):
                    controller.OtherOptions = "One Speed";
                    break;
                case "2" when (ProductClass == "w"):
                    controller.OtherOptions = "Two Speed";
                    break;
                case "a" when (ProductClass == "w"):
                    controller.OtherOptions = "Two Speed Temp Control, not water saver";
                    break;
                case "b" when (ProductClass == "w"):
                    controller.OtherOptions = "One Speed Temp Control, not water saver";
                    break;
                case "4" when (ProductClass == "w"):
                    controller.OtherOptions = "SS Top front load washer";
                    break;
                case "6" when (ProductClass == "w"):
                    controller.OtherOptions = "Six Speed or variable drive";
                    break;
                case "7" when (ProductClass == "w"):
                    controller.OtherOptions = "Large door, Horizon and stack washer/dryer";
                    break;
                case "8" when (ProductClass == "w"):
                    controller.OtherOptions = "Large door with SS top";
                    break;
                case "9" when (ProductClass == "w"):
                    controller.OtherOptions = "Variable drive and high efficiency setup";
                    break;
                case "0" when (ProductClass != "w"):
                    controller.OtherOptions = "Standard, No Window";
                    break;
                case "1" when (ProductClass != "w"):
                    controller.OtherOptions = "Window";
                    break;
                case "4" when (ProductClass != "w"):
                    controller.OtherOptions = "SS Top, Cyl. Light, No Window";
                    break;
                case "6" when (ProductClass != "w"):
                    controller.OtherOptions = "SS Top, No Window";
                    break;
                case "7" when (ProductClass != "w"):
                    controller.OtherOptions = "Large door, stack washer/dryer";
                    break;
                default:
                    controller.ResponseType = "failure";
                    return controller;
            }

            //Sixith Position - Washbasket, Heat
            switch (Washbasket)
            {
                case "0" when (ProductClass == "w"):
                    controller.Washbasket = "Blue Porcelain & pump";
                    break;
                case "1" when (ProductClass == "w"):
                    controller.Washbasket = "Stainless Steel & pump";
                    break;
                case "2" when (ProductClass == "w"):
                    controller.Washbasket = "Stainless Steel & pump & prep for supply injection";
                    break;
                case "3" when (ProductClass == "w"):
                    controller.Washbasket = "Stainless Steel & gravity drain";
                    break;
                case "4" when (ProductClass == "w"):
                    controller.Washbasket = "Stainless Steel,  pump, 3 phase heater in tub, CE";
                    break;
                case "5" when (ProductClass == "w"):
                    controller.Washbasket = "Stainless Steel, gravity drain, 3 phase w/heater, CE";
                    break;
                case "3" when (ProductClass != "w"):
                    controller.HeatSource = "Gravity drain (Stack W/D)";
                    break;
                case "5" when (ProductClass != "w"):
                    controller.HeatSource = "Gravity drain, 3 phase w/heater (Stack W/D)";
                    break;
                case "7" when (ProductClass != "w"):
                    controller.HeatSource = "Electric Heat";
                    break;
                case "9" when (ProductClass != "w"):
                    controller.HeatSource = "Gas Heat";
                    break;
                default:
                    controller.ResponseType = "failure";
                    return controller;
            }

            //Seventh Position - Color

            switch (Color)
            {
                case "w":
                    controller.Color = "White";
                    break;
                case "q":
                    controller.Color = "Bisque";
                    break;
                case "n":
                    controller.Color = "Stainless Steel";
                    break;
                default:
                    controller.ResponseType = "failure";
                    return controller;
            }


            //Eight Position - Design
            switch (Design)
            {
                case "f":
                    controller.Design = "1999 Badger dryer assembled in Ripon - including 3-piece cabinet - Basic Configuration";
                    break;
                case "g":
                    controller.Design = "1999 Badger dryer assembled in Ripon - including 3-piece cabinet - CE Configuration (electic & pre glow bar)";
                    break;
                case "j":
                    controller.Design = "Muskie II, Libra, Badger - Basic Configuration, Phase II Controls";
                    break;
                case "m":
                    controller.Design = "Libra= 710RPM & curved agitator; Badger= CE Configuration (glow bar ignition)";
                    break;
                case "n":
                    controller.Design = "Cyclone (Muskie III)= new drive system, Stack Washer (Muskie II)/Dryer=Basic Configuration";
                    break;
                default:
                    controller.ResponseType = "failure";
                    return controller;

            }
            controller.ResponseType = "success";
            return controller;
        }

        public static GPayKit GetGPayKit(string modelNumber)
        {
            GPayKit kit = new GPayKit();
            kit.AlternativePartNos = new List<GPayPartNo>();
            kit.NumberOfKitsNeeded = 1;
            kit.Supported = true;
            modelNumber = modelNumber.ToLower();
            char[] charArr = modelNumber.ToCharArray();

            var ControlOption = charArr[3].ToString();



            if (ControlOption.IndexOfAny(new char[] { 'a', 'b', 't', 'v', 'w', 'x' }) >= 0)
            {
                kit.RecommendedPartNo = GPayPartNo.MdcCoinKit;
            }
            else if (ControlOption.IndexOfAny(new char[] { 'c', 'f', 'y' }) >= 0)
            {
                kit.RecommendedPartNo = GPayPartNo.MdcCardKit;
            }
            else
            {
                kit.Supported = false;
            }
            return kit;
        }
    }
}
