﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GPayControllerDecoder.domain;
using GPayControllerDecoder.models;

namespace GPayControllerDecoder.machines
{
    public class AllianceSmallChassis15Char
    {
        public static ControllerResponse GetController(string modelNumber)
        {

            //string val = null;
            modelNumber = modelNumber.ToLower();
            char[] charArr = modelNumber.ToCharArray();
            ControllerResponse controller = new ControllerResponse();

            if (modelNumber.Length < 15)
            {
                controller.ResponseType = "failure";
                return controller;
            }

            //First Position - Brand
            switch (charArr[0].ToString())
            {
                case "b":
                    controller.Brand = "IPSO";
                    break;
                case "g":
                    controller.Brand = "Econowash";
                    break;
                case "h":
                    controller.Brand = "Huebsch";
                    break;
                case "j":
                    controller.Brand = "JLA";
                    break;
                case "k":
                    controller.Brand = "Continental Girbau";
                    break;
                case "n":
                    controller.Brand = "No Name";
                    break;
                case "p":
                    controller.Brand = "Primus";
                    break;
                case "r":
                    controller.Brand = "Coinmatic Revolution";
                    break;
                case "s":
                    controller.Brand = "Speed Queen";
                    break;
                case "t":
                    controller.Brand = "IFB";
                    break;
                case "u":
                    controller.Brand = "Unimac";
                    break;
                default:
                    controller.ResponseType = "failure";
                    return controller;
            }

            //Second Position - Product
            switch (charArr[1].ToString())
            {
                case "d":
                    controller.Product = "Dryer";
                    break;
                case "f":
                    controller.Product = "Front Load Washer";
                    break;
                case "s":
                    controller.Product = "Stacked Dryer/Dryer";
                    break;
                case "t":
                    controller.Product = "Stacked Washer/Dryer";
                    break;
                case "w":
                    controller.Product = "Top Load Washer";
                    break;
                default:
                    controller.ResponseType = "failure";
                    return controller;
            }

            //Third Position - Heater
            switch (charArr[2].ToString())
            {
                case "1":
                    controller.Heater = "3 phase washer, 1 phase electric dryer";
                    break;
                case "2":
                    controller.Heater = "3 phase washer, 1 phase gas dryer";
                    break;
                case "3":
                    controller.Heater = "3 phase washer/dryer";
                    break;
                case "b":
                    controller.Heater = "Boost Heat";
                    break;
                case "e":
                    controller.Heater = "Electric";
                    break;
                case "g":
                    controller.Heater = "Gas";
                    break;
                case "h":
                    controller.Heater = "Heated washer and electric dryer";
                    break;
                case "l":
                    controller.Heater = "LP Gas";
                    break;
                case "n":
                    controller.Heater = "None";
                    break;
                default:
                    controller.ResponseType = "failure";
                    return controller;
            }

            //Forth Position - Control
            if (charArr[3].ToString().IndexOfAny(new char[] { 'b', 'c', 'd', 'e', 'j', 'k', 'l', 'm', 'n', 's', 't', 'w', 'x' }) < 0)
            {
                controller.ResponseType = "failure";
                return controller;
            }
            else if(charArr[10].ToString() == "1" || charArr[10].ToString() == "2") {
                controller.ControllerType = "MDC (Quantum)";
            }
            else if (charArr[10].ToString() == "6")
            {
                switch (charArr[3].ToString())
                {
                    case "b":
                        controller.ControllerType = "T1";
                        break;
                    case "j":
                        controller.ControllerType = "T3";
                        break;
                    case "k" when (charArr[1].ToString() == "w"):
                        controller.ControllerType = "T1";
                        break;
                    case "k" when (charArr[1].ToString() != "w"):
                        controller.ControllerType = "T2";
                        break;
                    case "n":
                        controller.ControllerType = "T4";
                        break;
                    case "w":
                        controller.ControllerType = "T4";
                        break;
                    case "x":
                        controller.ControllerType = "T3";
                        break;
                    case "c":
                        controller.ControllerType = "MDC";
                        break;
                    case "d":
                        controller.ControllerType = "Centurion 4 (Quantum)";
                        break;
                    case "e":
                        controller.ControllerType = "HEC";
                        break;

                    case "l":
                        controller.ControllerType = "Centurion C4 (Quantum)";
                        break;
                    case "m":
                        controller.ControllerType = "Hybrid";
                        break;

                    case "s":
                    case "t":
                        controller.ControllerType = "Hybrid";
                        break;
                }
            } else {
                switch (charArr[3].ToString())
                {
                    case "b":
                        controller.ControllerType = "A1";
                        break;
                    case "c":
                        controller.ControllerType = "MDC";
                        break;
                    case "d" :
                        controller.ControllerType = "Centurion C4 (Quantum)";
                        break;
                    case "e":
                        controller.ControllerType = "HEC";
                        break;
                    case "j":
                        controller.ControllerType = "A3";
                        break;
                    case "k" when (charArr[1].ToString() == "w" ): // ***K control TLW is A1, all other K control are A2
                        controller.ControllerType = "A1";
                        break;
                    case "k" when (charArr[1].ToString() != "w"):
                        controller.ControllerType = "A2";
                        break;
                    case "l":
                        controller.ControllerType = "Centurion C4 (Quantum)";
                        break;
                    case "m":
                        controller.ControllerType = "Hybrid";
                        break;
                    case "n":
                        controller.ControllerType = "A4";
                        break;
                    case "s":
                    case "t":
                        controller.ControllerType = "Hybrid";
                        break;
                    case "w":
                        controller.ControllerType = "A4";
                        break;
                    case "x":
                        controller.ControllerType = "A3";
                        break;
                }
            }

            //Fith Position - Actuation

            switch (charArr[4].ToString())
            {
                case "c":
                    controller.Actuation = "Single drop installed";
                    break;
                case "d":
                    controller.Actuation = "Dual drop installed";
                    break;
                case "e":
                    controller.Actuation = "Electronic drop installed";
                    break;
                case "g":
                    controller.Actuation = "Programmed for OPL";
                    break;
                case "l":
                    controller.Actuation = "Prep for Central Pay";
                    break;
                case "m":
                    controller.Actuation = "OPL w/washer medical cycle";
                    break;
                case "n":
                    controller.Actuation = "None Coin";
                    break;
                case "r":
                    controller.Actuation = "Card installed (restricted use)";
                    break;
                case "x":
                    controller.Actuation = "Prep for coin";
                    break;
                case "y":
                    controller.Actuation = "Prep for card";
                    break;
                case "z":
                    controller.Actuation = "Prep for bill Acceptor (paper money)";
                    break;
                default:
                    controller.ResponseType = "failure";
                    return controller;
            }
            
            //Sixth Position - Speed
            switch (charArr[5].ToString())
            {
                case "1":
                    controller.Speed = "1 speed TLW";
                    break;
                case "2":
                    controller.Speed = "2 speed TLW";
                    break;
                case "a":
                    controller.Speed = "10° Front Console";
                    break;
                case "b":
                    controller.Speed = "45° Front Console";
                    break;
                case "f":
                    controller.Speed = "Flat Front Console";
                    break;
                case "r":
                    controller.Speed = "Rear Console";
                    break;
                default:
                    controller.ResponseType = "failure";
                    return controller;
            }


            //Seventh Position - Cylinder
            switch (charArr[6].ToString())
            {
                case "a":
                    controller.Cylinder = "Blue Porcelain, 2.9 cu. Ft";
                    break;
                case "b":
                    controller.Cylinder = "Polished SS rear blkd, SS cylinder";
                    break;
                case "c":
                    controller.Cylinder = "Polished SS rear blkd, Galv cylinder";
                    break;
                case "g":
                    controller.Cylinder = "Galvanized";
                    break;
                case "h":
                    controller.Cylinder = "Stainless 2.9 cu. Ft";
                    break;
                case "j":
                    controller.Cylinder = "SS with injection";
                    break;
                case "p":
                    controller.Cylinder = "Blue porcelain";
                    break;
                case "s":
                    controller.Cylinder = "Stainless";
                    break;
                case "w":
                    controller.Cylinder = "White";
                    break;
                default:
                    controller.ResponseType = "failure";
                    return controller;
            }


            //Eight Position - Door/Drain
            switch (charArr[7].ToString())
            {
                case "g":
                    controller.Door = "Gravity drain";
                    break;
                case "n":
                    controller.Door = "Natural drain";
                    break;
                case "p":
                    controller.Door = "Pump drain";
                    break;
                case "s":
                    controller.Door = "Solid Dryer Door";
                    break;
                case "w":
                    controller.Door = "Window door";
                    break;
                default:
                    controller.ResponseType = "failure";
                    return controller;

            }


            //11th Position - Design
            switch (charArr[10].ToString())
            {
                case "1":
                    controller.Design = "Current";
                    break;
                case "2":
                    controller.Design = "2013 DOE compliant TLW, 2013 Commercial FLW";
                    break;
                case "3":
                    controller.Design = "2015 ACA release";
                    break;
                case "4":
                    controller.Design = "2015/2016 ACA";
                    break;
                case "5":
                    controller.Design = "2018 ACA, DOE J2 in North America";
                    break;
                case "6":
                    controller.Design = "Quantum Gold Pro (Titanium)";
                    break;
                default:
                    controller.ResponseType = "failure";
                    return controller;
            }


            //12th Position - Market
            switch (charArr[11].ToString())
            {
                case "a":
                    controller.Market = "Australia";
                    break;
                case "b":
                    controller.Market = "Argentina";
                    break;
                case "c":
                    controller.Market = "Canada";
                    break;
                case "d":
                    controller.Market = "Inmetro Brazil";
                    break;
                case "e":
                    controller.Market = "UK";
                    break;
                case "f":
                    controller.Market = "Taiwan";
                    break;
                case "g":
                    controller.Market = "GSA";
                    break;
                case "h":
                    controller.Market = "China";
                    break;
                case "j":
                    controller.Market = "Japan";
                    break;
                case "k":
                    controller.Market = "Korea";
                    break;
                case "l":
                    controller.Market = "Thailand";
                    break;
                case "m":
                    controller.Market = "Mexico";
                    break;
                case "n":
                    controller.Market = "No agency";
                    break;
                case "p":
                    controller.Market = "Philippines";
                    break;
                case "q":
                    controller.Market = "Russian Federation";
                    break;
                case "r":
                    controller.Market = "Saudi Arabia";
                    break;
                case "s":
                    controller.Market = "South Africa";
                    break;
                case "t":
                    controller.Market = "Standard";
                    break;
                case "u":
                    controller.Market = "Europe/CE";
                    break;
                case "v":
                    controller.Market = "Vietnam";
                    break;
                case "w":
                    controller.Market = "Gulf States";
                    break;
                case "x":
                    controller.Market = "Singapore/Malaysia";
                    break;
                case "z":
                    controller.Market = "Universal IEC";
                    break;
                default:
                    controller.ResponseType = "failure";
                    return controller;

            }

            controller.ResponseType = "success";
            return controller;
        }
        public static GPayKit GetGPayKit(string modelNumber)
        {
            GPayKit kit = new GPayKit();
            kit.AlternativePartNos = new List<GPayPartNo>();

            string subType = "";
            kit.Supported = true;
            modelNumber = modelNumber.ToLower();
            char[] charArr = modelNumber.ToCharArray();

            var product = charArr[1].ToString();
            var controller = charArr[3].ToString();
            var actuation = charArr[4].ToString();
            var design = charArr[10].ToString();


            if (controller.IndexOfAny(new char[] {'d', 'e', 'm', 'r', 's', 't', 'y'}) >= 0)
            {
                kit.Supported = false;
                return kit;
            }
            if (actuation.IndexOfAny(new char[] { 'g', 'l', 'm', 'z' }) >= 0)
            {
                kit.Supported = false;
                return kit;
            }
            
            if (product.IndexOfAny(new char[] { 's', 't' }) >= 0)
            {
                kit.NumberOfKitsNeeded = 2;
            }
            else
            {
                kit.NumberOfKitsNeeded = 1;
            }
            if (design == "1" || design == "2")
            {
                subType = "MDC";
            }
            else if (design == "6")
            {
                subType = "QGP";
            }
            else
            {
                subType = "ACA";
            }

            if (subType == "ACA" && actuation.IndexOfAny(new char[] {'c', 'd', 'e', 'x'}) >= 0)
            {
                kit.RecommendedPartNo = GPayPartNo.AcaLinePower;
                kit.AlternativePartNos.Add(GPayPartNo.AcaWithOutPower);
            }
            else if (subType == "ACA" && actuation.IndexOfAny(new char[] { 'n', 'r', 'y' }) >= 0)
            {
                kit.RecommendedPartNo = GPayPartNo.Aca24vPower;
                kit.AlternativePartNos.Add(GPayPartNo.AcaWithOutPower);
            }
            else if (subType == "MDC" && actuation.IndexOfAny(new char[] { 'c', 'd', 'e', 'x' }) >= 0)
            {
                kit.RecommendedPartNo = GPayPartNo.MdcCoinKit;
            }
            else if (subType == "MDC" && actuation.IndexOfAny(new char[] { 'n', 'r', 'y' }) >= 0)
            {
                kit.RecommendedPartNo = GPayPartNo.MdcCardKit;
            }
            else if (subType == "QGP" && actuation.IndexOfAny(new char[] { 'c', 'd', 'e', 'x' }) >= 0)
            {
                kit.RecommendedPartNo = GPayPartNo.QgpLinePower;
                kit.AlternativePartNos.Add(GPayPartNo.QgpWithOutPower);
            }
            else if (subType == "QGP" && actuation.IndexOfAny(new char[] { 'n', 'r', 'y' }) >= 0)
            {
                kit.RecommendedPartNo = GPayPartNo.Qgp24vPower;
                kit.AlternativePartNos.Add(GPayPartNo.QgpWithOutPower);
            }
            return kit;
        }
    }
}
