﻿using GPayControllerDecoder.models;
using System;
using System.Collections.Generic;
using System.Text;

namespace GPayControllerDecoder.machines
{
    public class MDCWXCab13Char
    {
        public static ControllerResponse GetController(string modelNumber)
        {

            //string val = null;
            modelNumber = modelNumber.ToLower();
            char[] charArr = modelNumber.ToCharArray();
            ControllerResponse controller = new ControllerResponse();

            if (modelNumber.Length < 15)
            {
                controller.ResponseType = "failure";
                return controller;
            }

            //First Position - Brand
            switch (charArr[0].ToString())
            {
                case "i":
                    controller.Brand = "IPSO NA";
                    break;
                case "h":
                    controller.Brand = "Huebsch";
                    break;
                case "s":
                    controller.Brand = "Speed Queen";
                    break;
                case "u":
                    controller.Brand = "Unimac";
                    break;
                case "y":
                    controller.Brand = "Private Label";
                    break;
                default:
                    controller.ResponseType = "failure";
                    return controller;
            }

            //Second Position - Product
            switch (charArr[1].ToString())
            {
                case "c":
                    controller.Product = "Cabinet Hardmount";
                    break;
                default:
                    controller.ResponseType = "failure";
                    return controller;
            }

            //Third Position - Market
            switch (charArr[2].ToString())
            {
                case "n":
                    controller.Market = "North America Approval";
                    break;
                case "l":
                    controller.Market = "International with North America Approval";
                    break;
                case "u":
                    controller.Market = "International CE/EAC";
                    break;
                case "b":
                    controller.Market = "Brazil (CE)";
                    break;
                case "d":
                    controller.Market = "Daiwa (ETL)";
                    break;
                case "w":
                    controller.Market = "WRAS (CE)";
                    break;
                case "y":
                    controller.Market = "Australian Watermark (CE)";
                    break;
                case "z":
                    controller.Market = "No Agency International";
                    break;
                case "h":
                    controller.Market = "China - RoHS (ETL)";
                    break;
                default:
                    controller.ResponseType = "failure";
                    return controller;
            }

            //Seventh Position - Control
 
            switch (charArr[6].ToString())
            {
                case "g":
                    controller.ControllerType = "C1";
                    break;
                case "h":
                    controller.ControllerType = "C2";
                    break;
                case "j":
                    controller.ControllerType = "C2.5";
                    break;
                case "k":
                    controller.ControllerType = "C3";
                    break;
                case "w":
                    controller.ControllerType = "C4";
                    break;
                case "x":
                    controller.ControllerType = "C3";
                    break;
                default:
                    controller.ResponseType = "failure";
                    return controller;
        }

            //Eight Position - Actuation
            switch (charArr[7].ToString())
            {
                case "c":
                    controller.Actuation = "Coin Drop Meter";
                    break;
                case "d":
                    controller.Actuation = "Dual drop installed";
                    break;
                case "e":
                    controller.Actuation = "Electronic drop installed";
                    break;
                case "l":
                    controller.Actuation = "Central Pay";
                    break;
                case "n":
                    controller.Actuation = "None Coin";
                    break;
                case "x":
                    controller.Actuation = "Prep for Coin";
                    break;
                case "y":
                    controller.Actuation = "Prep for card";
                    break;
                default:
                    controller.ResponseType = "failure";
                    return controller;
            }
   
            //Ninth Position - Speed

            switch (charArr[8].ToString())
            {
                case "f":
                    controller.Speed = "100G";
                    break;
                case "v":
                    controller.Speed = "200G or 165G";
                    break;
                default:
                    controller.ResponseType = "failure";
                    return controller;
            }

            //Tenth Position - Voltage
            switch (charArr[9].ToString())
            {
                case "b":
                    controller.Voltage = "120/60/1, (L1, N)";
                    break;
                case "x":
                    controller.Voltage = "200-208/220-240/50/60/1 or 3 2W(L1,L2) or 2W(L1,N) or 3W(L1,L2,L3)";
                    break;
                case "q":
                    controller.Voltage = "200-208/220-240/50/60/3 - 3W(L1,L2,L3)";
                    break;
                case "p":
                    controller.Voltage = "380-415/50/60/3 - 3W(L1,L2,L3)";
                    break;
                case "n":
                    controller.Voltage = " 440-480/50/60/3 3W(L1,L2,L3)";
                    break;
                default:
                    controller.ResponseType = "failure";
                    return controller;
            }

            //Eleventh Position - Panels
            switch (charArr[10].ToString())
            {
                case "u":
                    controller.Door = "Standard";
                    break;
                case "1":
                    controller.Door = "SS Sides";
                    break;
                case "c":
                    controller.Door = "Chrome Door";
                    break;
                case "b":
                    controller.Door = "Chrome Dor & SS Sides";
                    break;
                default:
                    controller.ResponseType = "failure";
                    return controller;
            }

            //12th Position - Design
            switch (charArr[11].ToString())
            {
                case "3":
                    controller.Design = "Design 3";
                    break;
                case "2":
                    controller.Design = "Design 2";
                    break;
                case "1":
                    controller.Design = "Design 1";
                    break;
                case "0":
                    controller.Design = "Pilot";
                    break;
                case "d":
                    controller.Design = "Deviantion";
                    break;
                case "e":
                    controller.Design = "Engineering";
                    break;
                default:
                    controller.ResponseType = "failure";
                    return controller;

            }

            //13th Position - Heat feature
            switch (charArr[12].ToString())
            {
                case "0":
                    controller.Market = "Standard";
                    break;
                case "d":
                    controller.Market = "Direct Steam";
                    break;
                case "e":
                    controller.Market = "Electric";
                    break;
                case "p":
                    controller.Market = "Prep for Steam";
                    break;
                default:
                    controller.ResponseType = "failure";
                    return controller;
            }

            controller.ResponseType = "success";
            return controller;
        }
        public static GPayKit GetGPayKit(string modelNumber)
        {
            GPayKit kit = new GPayKit();
            kit.AlternativePartNos = new List<GPayPartNo>();
            kit.NumberOfKitsNeeded = 1;
            kit.Supported = true;
            modelNumber = modelNumber.ToLower();
            char[] charArr = modelNumber.ToCharArray();

            var actuation = charArr[7].ToString();


            if (actuation.IndexOfAny(new char[] { 'c', 'd', 'e', 'x' }) >= 0)
            {
                kit.RecommendedPartNo = GPayPartNo.MdcCoinKit;
            }
            else if (actuation.IndexOfAny(new char[] { 'n', 'l', 'y' }) >= 0)
            {
                kit.RecommendedPartNo = GPayPartNo.MdcCardKit;
            }
            else
            {
                kit.Supported = false;
            }

            return kit;
        }
    }
}

