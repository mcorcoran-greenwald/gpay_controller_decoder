﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GPayControllerDecoder.domain;
using GPayControllerDecoder.models;

namespace GPayControllerDecoder.machines
{
    public class Alliance18CharTumbler
    {
        public static ControllerResponse GetController(string modelNumber)
        {

            //string val = null;
            modelNumber = modelNumber.ToLower();
            char[] charArr = modelNumber.ToCharArray();
            ControllerResponse controller = new ControllerResponse();

            if (modelNumber.Length < 18)
            {
                controller.ResponseType = "failure";
                return controller;
            }

            //First Position - Brand
            switch (charArr[0].ToString())
            {
                case "b":
                    controller.Brand = "IPSO";
                    break;
                case "g":
                    controller.Brand = "Girbau";
                    break;
                case "h":
                    controller.Brand = "Huebsch";
                    break;
                case "k":
                    controller.Brand = "Continental";
                    break;
                case "m":
                    controller.Brand = "Private Label Speed Queen";
                    break;
                case "n":
                    controller.Brand = "No Name";
                    break;
                case "p":
                    controller.Brand = "Primus";
                    break;
                case "s":
                    controller.Brand = "Speed Queen";
                    break;
                case "u":
                    controller.Brand = "Unimac";
                    break;
                default:
                    controller.ResponseType = "failure";
                    return controller;
            }


            //Second Position - Market
            switch (charArr[1].ToString())
            {
                case "t":
                    controller.Market = "CSA North America";
                    break;
                case "g":
                    controller.Market = "CSA International";
                    break;
                case "a":
                    controller.Market = "Australia (IEC)";
                    break;
                case "h":
                    controller.Market = "China  (IEC)";
                    break;
                case "j":
                    controller.Market = "Japan  (IEC)";
                    break;
                case "k":
                    controller.Market = "Korea Gas  (IEC)";
                    break;
                case "u":
                    controller.Market = "Europe/CE";
                    break;
                case "l":
                    controller.Market = "Thailand TISI (IEC)";
                    break;
                default:
                    controller.ResponseType = "failure";
                    return controller;
            }


            //Seventh Position - Control
            switch (charArr[6].ToString())
            {
                case "b":
                    controller.ControllerType = "MDC II (Vend)";
                    break;
                case "n":
                    controller.ControllerType = "H7S (OPL or Vend)";
                    break;
                case "w":
                    controller.ControllerType = "H7S Networked (Vend)";
                    break;
                case "d":
                    controller.ControllerType = "MGD Networkable (OPL)";
                    break;
                case "f":
                    controller.ControllerType = "L7S (OPL)";
                    break;
                default:
                    controller.ResponseType = "failure";
                    return controller;
            }
           
            //Eight Position - Actuation
            switch (charArr[7].ToString())
            {
                case "n":
                    controller.Actuation = "OPL";
                    break;
                case "l":
                    controller.Actuation = "Prep for Central Pay";
                    break;
                case "x":
                    controller.Actuation = "Prep for Coin";
                    break;
                case "y":
                    controller.Actuation = "Prep for Card";
                    break;
                case "c":
                    controller.Actuation = "Single Coin";
                    break;
                case "d":
                    controller.Actuation = "Dual Coin";
                    break;
                case "e":
                    controller.Actuation = "Electronic Drop";
                    break;
                case "t":
                    controller.Actuation = "Token";
                    break;
                case "f":
                    controller.Actuation = "Single Coin + CD Lock & Key";
                    break;
                case "q":
                    controller.Actuation = "Dual Coin + CD Lock & Key";
                    break;
                case "h":
                    controller.Actuation = "Electronic Drop + CD Lock & Key";
                    break;
                case "v":
                    controller.Actuation = "Token + CD Lock & Key";
                    break;
                default:
                    controller.ResponseType = "failure";
                    return controller;
            }

            //Ninth Position - Coin Meter
            switch (charArr[8].ToString())
            {
                case "0":
                    controller.OtherOptions = "Coin Meter: Standard";
                    break;
                case "1":
                    controller.OtherOptions = "Coin Meter: CD Lock & Key";
                    break;
                default:
                    controller.ResponseType = "failure";
                    return controller;
            }



            controller.ResponseType = "success";
            return controller;
        }
        public static GPayKit GetGPayKit(string modelNumber)
        {
            GPayKit kit = new GPayKit();
            kit.AlternativePartNos = new List<GPayPartNo>();

            kit.Supported = true;
            modelNumber = modelNumber.ToLower();
            kit.NumberOfKitsNeeded = 1;
            kit.RecommendedPartNo = GPayPartNo.Qgp24vPower;
            kit.AlternativePartNos.Add(GPayPartNo.QgpWithOutPower);

            return kit;
        }
    }
}
