﻿using GPayControllerDecoder.models;
using System;
using System.Collections.Generic;
using System.Text;

namespace GPayControllerDecoder.machines
{
    public class AllianceQuantum
    {
        public static ControllerResponse GetController(string modelNumber)
        {
            //string val = null;
            modelNumber = modelNumber.ToLower();
            char[] charArr = modelNumber.ToCharArray();
            ControllerResponse controller = new ControllerResponse();

            if (modelNumber.Length < 9)
            {
                controller.ResponseType = "failure";
                return controller;
            }

            var brand = charArr[0].ToString();
            var product = charArr[1].ToString();
            var market = charArr[2].ToString();
            var capactiy = string.Concat(charArr[3].ToString(), charArr[4].ToString(), charArr[5].ToString());
            var control = charArr[6].ToString();
            var actuation = charArr[7].ToString();
            var speed = charArr[8].ToString();
            var voltage = charArr[9].ToString();


            switch (brand)
            {
                case "h":
                    controller.Brand = "Huebsch";
                    break;
                case "s":
                    controller.Brand = "Speed Queen";
                    break;
                case "u":
                    controller.Brand = "Unimac";
                    break;
                default:
                    controller.ResponseType = "failure";
                    return controller;
            }


            switch (product)
            {
                case "c":
                    controller.Product = "Cabinet Hardmount";
                    break;
                default:
                    controller.ResponseType = "failure";
                    return controller;
            }


            switch (market)
            {
                case "n":
                    controller.Market = "Domestic ETL(c)";
                    break;
                case "l":
                    controller.Market = "International ETL(c)";
                    break;
                case "u":
                    controller.Market = "International CE";
                    break;
                default:
                    controller.ResponseType = "failure";
                    return controller;
            }
  

            switch (control)
            {
                case "g":
                    controller.ControllerType = "C1 (Q Std & G 200)";
                    break;
                case "h":
                    controller.ControllerType = "C2 (G 400)";
                    break;
                case "j":
                    controller.ControllerType = "C2.5 (Q Silver)";
                    break;
                case "k":
                    controller.ControllerType = "C3 (G 600)";
                    break;
                case "l":
                    controller.ControllerType = "C4 (Q Gold)";
                    break;
                case "w":
                    controller.ControllerType = "C4 Network (Q Gold/NM)";
                    break;
                default:
                    controller.ResponseType = "failure";
                    return controller;
            }


            switch (actuation)
            {
                case "c":
                    controller.Actuation = "Drop Coin Meter";
                    break;
                case "d":
                    controller.Actuation = "Dual Drop";
                    break;
                case "e":
                    controller.Actuation = "Electronic World Drop";
                    break;
                case "l":
                    controller.Actuation = "Prep for Remote / Central Pay";
                    break;
                case "n":
                    controller.Actuation = "Non Coin (OPL)";
                    break;
                case "x":
                    controller.Actuation = "Prep For Coin Meter";
                    break;
                case "y":
                    controller.Actuation = "Prep For Card Reader";
                    break;
                default:
                    controller.ResponseType = "failure";
                    return controller;
            }
   

            switch (speed)
            {
                case "2":
                    controller.Speed = "2 Speed Standard Spin";
                    break;
                case "f":
                    controller.Speed = "Inverter/Standard Spin";
                    break;
                case "v":
                    controller.Speed = "Inverter/Mixed Spin";
                    break;
                default:
                    controller.ResponseType = "failure";
                    return controller;
            }


            switch (voltage)
            {
                case "b":
                    controller.Voltage = "120/60/1/2 (20lb 2sp only)";
                    break;
                case "c":
                    controller.Voltage = "380-415/50/3/4 (2 sp 20-60 Only)";
                    break;
                case "f":
                    controller.Voltage = "440-480/60/3/3 (2 sp 20-60 only)";
                    break;
                case "x":
                    controller.Voltage = "200-208/220 - 240/50/60/1/3 (F & V Speed Only)";
                    break;
                case "q":
                    controller.Voltage = "200-208/220 - 240/50/60/3 (F & V Speed Only)";
                    break;
                case "r":
                    controller.Voltage = "380-415/440 - 480/50/60/3  (F & V Speed Only)";
                    break;
                default:
                    controller.ResponseType = "failure";
                    return controller;
            }

            controller.ResponseType = "success";
            return controller;
        }

        public static GPayKit GetGPayKit(string modelNumber)
        {
            GPayKit kit = new GPayKit();
            kit.AlternativePartNos = new List<GPayPartNo>();

            
            kit.Supported = true;
            kit.RecommendedPartNo = GPayPartNo.AcaWithOutPower;
            modelNumber = modelNumber.ToLower();
            
            return kit;
        }
    }
}
