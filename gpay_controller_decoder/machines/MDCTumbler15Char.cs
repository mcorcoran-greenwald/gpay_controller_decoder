﻿using GPayControllerDecoder.models;
using System;
using System.Collections.Generic;
using System.Text;

namespace GPayControllerDecoder.machines
{
    public class MDCTumbler15Char
    {
        public static ControllerResponse GetController(string modelNumber)
        {

            //string val = null;
            modelNumber = modelNumber.ToLower();
            char[] charArr = modelNumber.ToCharArray();
            ControllerResponse controller = new ControllerResponse();

            if (modelNumber.Length < 15)
            {
                controller.ResponseType = "failure";
                return controller;
            }

            //First Position - Brand
            switch (charArr[0].ToString())
            {
                case "b":
                    controller.Brand = " IPSO International";
                    break;
                case "g":
                    controller.Brand = "Girbau";
                    break;
                case "h":
                    controller.Brand = "Huebsch";
                    break;
                case "i":
                    controller.Brand = "IPSO USA";
                    break;
                case "k":
                    controller.Brand = "Continental";
                    break;
                case "l":
                    controller.Brand = "Lavamac";
                    break;
                case "m":
                    controller.Brand = "Private Label Speed Queen";
                    break;
                case "n":
                    controller.Brand = "Non-Branded";
                    break;
                case "p":
                    controller.Brand = "Primus";
                    break;
                case "s":
                    controller.Brand = "Speed Queen";
                    break;
                case "u":
                    controller.Brand = "Unimac";
                    break;
                default:
                    controller.ResponseType = "failure";
                    return controller;
            }

            //Sixth Position - Heater
            switch (charArr[5].ToString())
            {
                case "n":
                    controller.Heater = "Natural Gas";
                    break;
                case "l":
                    controller.Heater = "Liquid Propane";
                    break;
                case "e":
                    controller.Heater = "Electric";
                    break;
                case "s":
                    controller.Heater = "Steam";
                    break;
                case "d":
                    controller.Heater = "Japan - LP";
                    break;
                default:
                    controller.ResponseType = "failure";
                    return controller;
            }

            //Seventh Position - Control
            switch (charArr[6].ToString())
            {
                case "b":
                    controller.ControllerType = "Basic Electronic Control ";
                    break;
                case "l":
                    controller.ControllerType = "Quantum Gold - Network Adaptable";
                    break;
                case "w":
                    controller.ControllerType = "Quantum Gold - Network Ready";
                    break;
                case "k":
                    controller.ControllerType = "Galaxy 600 - Network Adaptable";
                    break;
                case "z":
                    controller.ControllerType = "Galaxy 600 - Network Ready";
                    break;
                case "3" when (charArr[7].ToString() != "o"):
                    controller.ControllerType = "DX4 Vended";
                    break;
                case "3" when (charArr[7].ToString() == "o"):
                    controller.ControllerType = "DX4 OPL";
                    break;
                default:
                    controller.ResponseType = "failure";
                    return controller;
            }


            //Eigth Position - Actuation
            switch (charArr[7].ToString())
            {
                case "v":
                case "c":
                    controller.Actuation = "Single Coin";
                    break;
                case "x":
                    controller.Actuation = "Prep for Coin";
                    break;
                case "y":
                    controller.Actuation = "Prep for Card";
                    break;
                case "l":
                    controller.Actuation = "Prep for Central Pay";
                    break;
                default:
                    controller.ResponseType = "failure";
                    return controller;
            }


            controller.ResponseType = "success";
            return controller;
        }
        public static GPayKit GetGPayKit(string modelNumber)
        {
            GPayKit kit = new GPayKit();
            kit.AlternativePartNos = new List<GPayPartNo>();
            kit.NumberOfKitsNeeded = 1;

            kit.Supported = false;
            modelNumber = modelNumber.ToLower();
            char[] charArr = modelNumber.ToCharArray();

            var stack = charArr[2].ToString();
            var actuation = charArr[7].ToString();

            if (stack.IndexOfAny(new char[] { 't' }) >= 0)
            {
                kit.NumberOfKitsNeeded = 2;
            }
            if (actuation.IndexOfAny(new char[] { 'c', 'v', 'x' }) >= 0)
            {
                kit.Supported = true;
                kit.RecommendedPartNo = GPayPartNo.MdcCoinKit;

                return kit;
            }

            if (actuation.IndexOfAny(new char[] { 'y' }) >= 0)
            {
                kit.Supported = true;
                kit.RecommendedPartNo = GPayPartNo.MdcCardKit;

                return kit;
            }

            return kit;
        }
    }
}

