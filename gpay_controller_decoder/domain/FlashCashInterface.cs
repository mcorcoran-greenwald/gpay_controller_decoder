﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using GPayControllerDecoder.models;
using System.Threading;
using System.Linq;

namespace GPayControllerDecoder.domain
{
    public class FlashCashInterface
    {
        private static async Task<CloudTable> GetTable()
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(Environment.GetEnvironmentVariable("GIWebConnString", EnvironmentVariableTarget.Process));
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
            CloudTable table = tableClient.GetTableReference("flashCashKitLookup");
            await table.CreateIfNotExistsAsync();
            return table;
        }
        public static async Task<List<FlashCashKitEntity>> GetFlashCashModelNumbers(string brand)
        {
            CloudTable table = await GetTable();
            var items = new List<FlashCashKitEntity>();
            TableContinuationToken token = null;
            string filter = TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, brand);
            TableQuery<FlashCashKitEntity> query = new TableQuery<FlashCashKitEntity>().Where(filter);

            CancellationToken ct = default(CancellationToken);
            do
            {

                TableQuerySegment<FlashCashKitEntity> seg = await table.ExecuteQuerySegmentedAsync<FlashCashKitEntity>(query, token);

                token = seg.ContinuationToken;
                items.AddRange(seg);

            } while (token != null && !ct.IsCancellationRequested);           
            return items;
        }
        public async static Task<FlashCashKit> GetFlashCashKit(string modelNumber, string brand)
        {
            var kit = new FlashCashKit();
            kit.Supported = false;
            modelNumber = modelNumber.ToLower();
            var modelNumbers = await FlashCashInterface.GetFlashCashModelNumbers(brand);

            //Treat a lowercase 'x' like a wild card
            var wildCardModelNumber = modelNumbers.Where(x => x.RowKey.Contains("x", StringComparison.Ordinal));
            foreach (var model in wildCardModelNumber)
            {
                var modelNoArr = model.RowKey.Split('x');
                var firstPart = modelNoArr[0].ToLower();
                var secondPart = modelNoArr[1].ToLower();


                if (modelNumber.Substring(0, firstPart.Length) == firstPart && modelNumber
                        .Substring(firstPart.Length + 1).StartsWith(secondPart))
                {
                    kit.RecommendedPartNo = model.Kit;
                    kit.Supported = true;
                    return kit;
                }

            }

            var completeModelNumber = modelNumbers.Where(x => !x.RowKey.Contains("x", StringComparison.Ordinal));
            foreach (var model in completeModelNumber)
            {

                if (modelNumber.StartsWith(model.RowKey.ToLower()))
                {
                    kit.RecommendedPartNo = model.Kit;
                    kit.Supported = true;
                    return kit;
                }
            }

            return kit;

        }
    }


}
