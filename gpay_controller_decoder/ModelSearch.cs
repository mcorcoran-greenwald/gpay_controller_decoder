using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using GPayControllerDecoder.domain;
using GPayControllerDecoder.machines;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing.Constraints;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace GPayControllerDecoder
{
    public static class ControllerType
    {
        [FunctionName("ModelSearch")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)]
            HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP Controller Type Trigger function processed a request.");
            var response = new ControllerResponse();


            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            dynamic data = JsonConvert.DeserializeObject(requestBody);
            string modelNumber = data?.modelNumber;
            string brand = data?.brand;

            if (string.IsNullOrEmpty(modelNumber))
            {
                response.ResponseType = "failure";
                response.ErrorMessage = "Model Number is required";
                return new OkObjectResult(response);
            }




            if (brand == "alliance" || brand ==  "quantum")
            {

                response = AllianceSmallChassis15Char.GetController(modelNumber);
                if (response.ResponseType == "success")
                {
                    response.GPayKit = AllianceSmallChassis15Char.GetGPayKit(modelNumber);
                    response.FlashCashKit = await FlashCashInterface.GetFlashCashKit(modelNumber, brand);
                }

                if (response.ResponseType != "success")
                {
                    response = AllianceQuantum.GetController(modelNumber);
                    if (response.ResponseType == "success")
                    {
                        response.GPayKit = AllianceQuantum.GetGPayKit(modelNumber);
                    }
                }

                if (response.ResponseType != "success")
                {

                    response = AllianceOld8Char.GetController(modelNumber);
                    if (response.ResponseType == "success")
                    {
                        response.GPayKit = AllianceOld8Char.GetGPayKit(modelNumber);
                    }
                }

                if (response.ResponseType != "success")
                {

                    response = MDCDropMachines.GetController(modelNumber);
                    if (response.ResponseType == "success")
                    {
                        response.GPayKit = MDCDropMachines.GetGPayKit(modelNumber);
                    }
                }

                if (response.ResponseType != "success")
                {

                    response = MDCTumbler15Char.GetController(modelNumber);
                    if (response.ResponseType == "success")
                    {
                        response.GPayKit = MDCTumbler15Char.GetGPayKit(modelNumber);
                    }
                }
                if (response.ResponseType != "success")
                {

                    response = MDCWXCab13Char.GetController(modelNumber);
                    if (response.ResponseType == "success")
                    {
                        response.GPayKit = MDCWXCab13Char.GetGPayKit(modelNumber);
                    }
                }
                if (response.ResponseType != "success")
                {

                    response = Alliance18CharTumbler.GetController(modelNumber);
                    if (response.ResponseType == "success")
                    {
                        response.GPayKit = Alliance18CharTumbler.GetGPayKit(modelNumber);
                    }
                }
                if (response.ResponseType != "success")
                {

                    response = Alliance18CharWXCab.GetController(modelNumber);
                    if (response.ResponseType == "success")
                    {
                        response.GPayKit = Alliance18CharWXCab.GetGPayKit(modelNumber);
                    }
                }
            }
                                 
            if (brand == "maytag")
            {

                response = MayTag.GetController(modelNumber);

                if (response.ResponseType == "success")
                {
                    response.GPayKit = MayTag.GetGPayKit(modelNumber);
                    response.FlashCashKit = await FlashCashInterface.GetFlashCashKit(modelNumber, brand);
                }
            }

            if (brand == "lg")
            {

                response = LG.GetController(modelNumber);

                if (response.ResponseType == "success")
                {
                    response.GPayKit = LG.GetGPayKit(modelNumber);
                    response.FlashCashKit = await FlashCashInterface.GetFlashCashKit(modelNumber, brand);
                }
            }

            if (brand == "whirlpool")
            {

                response = Whirlpool.GetController(modelNumber);

                if (response.ResponseType == "success")
                {
                    response.GPayKit = Whirlpool.GetGPayKit(modelNumber);
                    response.FlashCashKit = await FlashCashInterface.GetFlashCashKit(modelNumber, brand);
                }
            }

            if (response.ResponseType == "success")
            {
                return new OkObjectResult(response);
            }


            //Controller Type not found
            response = new ControllerResponse
            {
                ResponseType = "failure",
                ErrorMessage =
                "Model Number does not match known structure, please contact Greenwald for more information."
            };
            return new OkObjectResult(response);
        }

        private static bool ValidSerialNumber(string serialNumber)
        {
            int i = 0;
            if (string.IsNullOrEmpty(serialNumber))
            {
                return false;
            }

            if (!int.TryParse(serialNumber, out i))
            {
                return false;
            }

            return true;
        }
    }

}
